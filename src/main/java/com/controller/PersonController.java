package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.beans.Person;
import com.dao.PersonDao;

@Controller
public class PersonController {
	
	@Autowired
    PersonDao dao;//will inject dao from xml file
	
	@RequestMapping(value = "/person", method = RequestMethod.GET)
	public ModelAndView student() {
		return new ModelAndView("person","command",new Person());
	}
	
	 @RequestMapping(value = "/addPerson", method = RequestMethod.POST)
	    public String addPerson(@ModelAttribute("SpringWeb")Person person, ModelMap model) {
	        model.addAttribute("name", person.getName());
	        model.addAttribute("age", person.getAge());
	        model.addAttribute("id", person.getId());
	        
	       int res =  dao.saveStudent(person);
	       if(res == 1) {
	    	   System.out.println("entro 1");
	       }else {
	    	   System.out.println(res + " <----valor res");
	       }
	        
	        return "newPerson";
	    }
	 
	   @RequestMapping("/viewStudents")
	    public String viewemp(Model m){
	        List<Person> list=dao.getStudents();
	        m.addAttribute("list",list);
	        return "viewstudents";
	    }
	   
	   /* It updates model object. */
	    /* It updates model object. */
	    @RequestMapping(value="/update",method = RequestMethod.POST)
	    public String update(@ModelAttribute("emp") Person emp){
	    	System.out.println("respuesta: " +emp.getName() + emp.getAge() + emp.getId());
	       int res =  dao.update(emp);
	       if(res == 1)
	       {
	    	   System.out.println("entro " + res);
	       }else {
	    	   System.out.println("error" + res);
	       }
	        return "redirect:/viewStudents";
	    }
	    /* It deletes record for the given id in URL and redirects to /viewemp */
	    @RequestMapping(value="/delete/{id}")
	    public String delete(@PathVariable int id){
	        dao.delete(id);
	        return "redirect:/viewStudents";
	    }
	    
	    /* It displays object data into form for the given id.
	     * The @PathVariable puts URL data into variable.*/
	    @RequestMapping(value="/editStudent/{id}")
	    public String edit(@PathVariable int id, Model m){
	        Person emp = dao.getStudentById(id);
	        m.addAttribute("student",emp);
	        return "studentEditForm";
	    }

}
