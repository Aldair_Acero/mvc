<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<%@ page import = "com.beans.Person" %> 
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<title>Edit Student</title>
</head>
<body>
	    <h1>Student Information ${student}</h1>
    <form method="POST" action="http://localhost:8080/SpringMVCapp/update" class="form-group">
    
        Name: <input name="name" value="${student.getName()}" class="form-control" type="text" />
        Age: <input name="age" value="${student.getAge()}" class="form-control" type="text" />
        ID: <input name="id" value="${student.getId()}" class="form-control" type="text" />
        <button class="btn btn-primary mt-3" type="submit">Update</button>   
        
    </form>

</body>
</html>