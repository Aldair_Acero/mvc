<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<%@ page import = "com.beans.Person" %> 


    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<title>viewStudents</title>
</head>
<body>

<h1 class="display-4 my-5">Students</h1>

<table class="container bordered table table-bordered my-3">

	<thead>
		<tr>
			<td>Id</td>
			<td>Name</td>
			<td>Age</td>
			<td></td>
			<td></td>
		</tr>
	</thead>
<c:forEach items="${list}" var="item">
    <tr>
		<td>${item.getId()}</td>
		<td>${item.getName()}</td>
		<td>${item.getAge()}</td>
		
		<td><a href="editStudent/${item.getId()}" class="btn btn-info">Update</a></td>
		<td><a href="delete/${item.getId()}" class="btn btn-danger">Delete</a></td>
		
	</tr>
</c:forEach>
</table>

<a href="person" class="btn btn-success">Add Student</a>
</body>
</html>