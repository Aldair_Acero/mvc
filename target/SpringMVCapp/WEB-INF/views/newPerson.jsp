<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
<%@ page import = "com.beans.Person" %>  

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<title>Insert title here</title>
</head>
<body>
<%
	System.out.println();
%>
<h1 class="display-4">New Person</h1>

<table class="container table">
<tr>
	<td>id: ${id}</td>
</tr>
<tr>
	<td>name: ${name}</td>
</tr>
<tr>
	<td>age: ${age}</td>
</tr>
</table>

<a href="viewStudents" class="btn btn-success">All students</a>

</body>
</html>