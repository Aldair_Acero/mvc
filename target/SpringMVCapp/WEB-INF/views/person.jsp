<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri ="http://www.springframework.org/tags/form" prefix="form" %>
<html>
<head>
    <title>Spring MVC Form</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <h1>Student Information</h1>
    <form:form method="post" action="addPerson" class="form-group">
        <form:label for="name" path="name">Name:</form:label>
        <form:input path="name" type="text" name="name" id="name" class="form-control"/>
        <form:label path="age" for="age">Age:</form:label>
        <form:input path="age" type="text" name="age" id="age" class="form-control"/>
        <form:label path="id" for="id">id:</form:label>
        <form:input path="id" type="text" name="id" id="id" class="form-control"/>
        <div align="center">
            <button class="btn btn-primary mt-3" type="submit">Submit</button>
        </div>
    </form:form>
    
    <a href="viewStudents" class="btn btn-success">All Students</a>
    
</div>
</body>
</html>